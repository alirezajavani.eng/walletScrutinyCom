---
title: "Pillar Wallet - Crypto Made Easy & Free"
altTitle: 

users: 10000
appId: com.pillarproject.wallet
launchDate: 2018-12-13
latestUpdate: 2020-06-10
apkVersionName: "2.9.2"
stars: 3.9
ratings: 400
reviews: 213
size: 44M
website: https://pillarproject.io
repository: https://github.com/pillarwallet/pillarwallet
issue: 
icon: com.pillarproject.wallet.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-02-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: PillarWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.pillarproject.wallet/
  - /posts/com.pillarproject.wallet/
---


This app is not a Bitcoin wallet. Neither the description nor the website claim
support of BTC and when installing it, you can find tokens with "Bitcoin" in
their name that can be managed with this app but none of them actually is Bitcoin.