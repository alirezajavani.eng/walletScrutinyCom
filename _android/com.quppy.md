---
title: "Quppy Wallet - bitcoin, crypto and euro payments"
altTitle: 

users: 50000
appId: com.quppy
launchDate: 
latestUpdate: 2020-12-22
apkVersionName: "1.0.41"
stars: 4.3
ratings: 2000
reviews: 951
size: 15M
website: https://quppy.com
repository: 
issue: 
icon: com.quppy.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: QuppyPay
providerLinkedIn: company/quppy
providerFacebook: quppyPay
providerReddit: 

redirect_from:
  - /com.quppy/
---


This provider loses no word on security or where the keys are stored. We assume
it is a custodial offering and therefore **not verifiable**.