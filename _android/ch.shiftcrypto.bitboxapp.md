---
title: "BitBoxApp"
altTitle: 

users: 1000
appId: ch.shiftcrypto.bitboxapp
launchDate: 
latestUpdate: 2020-12-12
apkVersionName: "android-4.24.1"
stars: 4.6
ratings: 14
reviews: 8
size: 63M
website: 
repository: 
issue: 
icon: ch.shiftcrypto.bitboxapp.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-08-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /ch.shiftcrypto.bitboxapp/
---


