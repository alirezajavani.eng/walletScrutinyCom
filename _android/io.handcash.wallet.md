---
title: "HandCash"
altTitle: 

users: 5000
appId: io.handcash.wallet
launchDate: 2019-09-10
latestUpdate: 2020-06-11
apkVersionName: "2.1.4"
stars: 4.2
ratings: 226
reviews: 167
size: 31M
website: https://handcash.io
repository: 
issue: 
icon: io.handcash.wallet.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: handcashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.handcash.wallet/
  - /posts/io.handcash.wallet/
---


A BSV wallet.