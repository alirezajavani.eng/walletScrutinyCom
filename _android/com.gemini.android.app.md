---
title: "Gemini: Buy Bitcoin Instantly"
altTitle: 

users: 500000
appId: com.gemini.android.app
launchDate: 
latestUpdate: 2021-01-15
apkVersionName: "2.30.2"
stars: 3.3
ratings: 2267
reviews: 1185
size: Varies with device
website: https://gemini.com
repository: 
issue: 
icon: com.gemini.android.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: gemini
providerLinkedIn: company/geminitrust
providerFacebook: GeminiTrust
providerReddit: 

redirect_from:
  - /com.gemini.android.app/
  - /posts/com.gemini.android.app/
---


This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.