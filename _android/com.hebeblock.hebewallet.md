---
title: "Hebe Wallet"
altTitle: 

users: 1000
appId: com.hebeblock.hebewallet
launchDate: 
latestUpdate: 2021-01-07
apkVersionName: "1.2.41"
stars: 3.4
ratings: 5
reviews: 3
size: 39M
website: 
repository: 
issue: 
icon: com.hebeblock.hebewallet.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.hebeblock.hebewallet/
  - /posts/com.hebeblock.hebewallet/
---


This page was created by a script from the **appId** "com.hebeblock.hebewallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.hebeblock.hebewallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.