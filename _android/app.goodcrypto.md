---
title: "Good Crypto: one trading app for all exchanges"
altTitle: 

users: 10000
appId: app.goodcrypto
launchDate: 
latestUpdate: 2020-11-05
apkVersionName: "1.5"
stars: 4.4
ratings: 123
reviews: 67
size: 17M
website: https://goodcrypto.app
repository: 
issue: 
icon: app.goodcrypto.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: false
signer: 
reviewArchive:


providerTwitter: GoodCryptoApp
providerLinkedIn: company/goodcrypto
providerFacebook: GoodCryptoApp
providerReddit: GoodCrypto

redirect_from:
  - /app.goodcrypto/
---


This app allows you to connect to accounts on trading platforms and does not
work as a wallet as such. We assume that you cannot receive Bitcoins to and send
them from this app.