---
title: "Remitano - Buy & Sell Bitcoin Fast & Securely"
altTitle: 

users: 100000
appId: com.remitano.remitano
launchDate: 
latestUpdate: 2021-01-15
apkVersionName: "5.4.1"
stars: 4.4
ratings: 9940
reviews: 4646
size: 30M
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: company/Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:
  - /com.remitano.remitano/
  - /posts/com.remitano.remitano/
---


This app is an interface to an exchange which holds your coins. On Google Play
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.