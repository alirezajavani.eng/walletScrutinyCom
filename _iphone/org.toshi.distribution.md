---
title: "Coinbase Wallet"
altTitle: 

appId: org.toshi.distribution
idd: 1278383455
released: 2017-09-27
updated: 2020-12-18
version: "23.2"
score: 4.70373
reviews: 15668
size: 99290112
developerWebsite: https://wallet.coinbase.com
repository: 
issue: 
icon: org.toshi.distribution.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-04
reviewStale: false
signer: 
reviewArchive:


providerTwitter: CoinbaseWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the iPhone version of the
[Android Coinbase Wallet — Crypto Wallet & DApp Browser](/android/org.toshi).

Just like the Android version, this wallet is **not verifiable**.