---
title: "Swap Wallet"
altTitle: 

appId: com.swap.wallet
idd: 1478737068
released: 2019-09-27
updated: 2020-12-24
version: "2.9.0"
score: 4.73332
reviews: 30
size: 26844160
developerWebsite: https://swapwallet.com
repository: 
issue: 
icon: com.swap.wallet.jpg
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-28
reviewStale: false
signer: 
reviewArchive:
- date: 2020-12-22
  version: 
  apkHash: 
  gitRevision: eceaf4f532a049d544a5f7ce8eda0f29b30e6fcf
  verdict: custodial


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

We can't find the app on the App Store anymore and assume it stopped
to exist.
