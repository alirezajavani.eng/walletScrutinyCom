---
title: "ZelCore"
altTitle: 

appId: com.zelcash.zelcore
idd: 1436296839
released: 2018-09-23
updated: 2020-12-31
version: "v4.2.0"
score: 4.5625
reviews: 48
size: 73672704
developerWebsite: https://zel.network/zelcore
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

