---
title: "Binance.US - Bitcoin & Crypto"
altTitle: 

appId: us.binance.fiat
idd: 1492670702
released: 2020-01-05
updated: 2021-01-15
version: "2.2.1"
score: 4.44142
reviews: 8364
size: 115012608
developerWebsite: https://www.binance.us
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: company/binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:

---

This is the iPhone version of [this Android app](/android/com.binance.us) and we
come to the same conclusion for the same reasons. This app is **not verifiable**.
