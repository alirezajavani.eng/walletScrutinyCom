---
title: "Bitnovo - Crypto Wallet"
altTitle: 

appId: com.bitnovo.app
idd: 1220883632
released: 2017-05-19
updated: 2020-12-02
version: "2.8.3"
score: 1
reviews: 3
size: 75660288
developerWebsite: https://www.bitnovo.com
repository: 
issue: 
icon: com.bitnovo.app.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

