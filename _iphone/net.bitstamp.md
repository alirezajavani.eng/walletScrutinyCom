---
title: "Bitstamp – crypto exchange app"
altTitle: 

appId: net.bitstamp
idd: 1406825640
released: 2019-01-30
updated: 2021-01-13
version: "1.6.4"
score: 4.81982
reviews: 3641
size: 82498560
developerWebsite: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

