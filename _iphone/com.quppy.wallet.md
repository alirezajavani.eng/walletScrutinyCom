---
title: "Quppy – Secure Bitcoin Wallet"
altTitle: 

appId: com.quppy.wallet
idd: 1417802076
released: 2018-08-09
updated: 2020-12-23
version: "1.0.41"
score: 4.94564
reviews: 368
size: 45872128
developerWebsite: https://quppy.com
repository: 
issue: 
icon: com.quppy.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

