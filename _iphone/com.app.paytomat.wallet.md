---
title: "Paytomat Wallet"
altTitle: 

appId: com.app.paytomat.wallet
idd: 1415300709
released: 2018-08-12
updated: 2021-01-17
version: "1.37.1"
score: 4.55
reviews: 20
size: 66301952
developerWebsite: https://paytomat.com
repository: 
issue: 
icon: com.app.paytomat.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

