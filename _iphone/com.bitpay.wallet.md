---
title: "BitPay – Buy Crypto"
altTitle: 

appId: com.bitpay.wallet
idd: 1149581638
released: 2016-10-24
updated: 2020-12-31
version: "11.2.13"
score: 4.07231
reviews: 968
size: 86063104
developerWebsite: https://bitpay.com
repository: 
issue: 
icon: com.bitpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

