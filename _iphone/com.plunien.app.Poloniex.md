---
title: "Poloniex Crypto Exchange"
altTitle: 

appId: com.plunien.app.Poloniex
idd: 1234141021
released: 2017-05-14
updated: 2018-10-04
version: "1.13.8"
score: 4.61301
reviews: 1951
size: 104064000
developerWebsite: https://www.poloniex.com
repository: 
issue: 
icon: com.plunien.app.Poloniex.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

