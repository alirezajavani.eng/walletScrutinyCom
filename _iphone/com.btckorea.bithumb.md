---
title: "Bithumb"
altTitle: 

appId: com.btckorea.bithumb
idd: 1299421592
released: 2017-12-05
updated: 2020-11-18
version: "1.4.5"
score: 2.5
reviews: 12
size: 14885888
developerWebsite: https://en.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

