---
title: "Huobi - Buy & Sell Bitcoin"
altTitle: 

appId: com.huobi.appStoreHuobiSystem
idd: 1023263342
released: 2015-08-19
updated: 2021-01-14
version: "6.1.0"
score: 4.82397
reviews: 2812
size: 204646400
developerWebsite: http://www.hbg.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

