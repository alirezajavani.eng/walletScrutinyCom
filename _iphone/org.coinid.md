---
title: "COINiD Vault"
altTitle: 

appId: org.coinid
idd: 1362831898
released: 2018-10-02
updated: 2020-01-23
version: "1.5.4"
score: 5
reviews: 4
size: 12781568
developerWebsite: https://coinid.org
repository: https://github.com/COINiD/COINiDVault
issue: 
icon: org.coinid.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-21
reviewStale: false
signer: 
reviewArchive:


providerTwitter: COINiDGroup
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This provider states in the App Store description:

> Check out the offline device guide on our website.

but forgot to provide a website. We'll go with the one provided for their
[Android version](/android/org.coinid.vault) as there is not much information to
be found on the App Store.

On their code repository we find this:

> Proper readme coming soon.

and this: 

> We are currently not including the secrets.js in the repo.

which means there is no full build instructsions and the app is therefore
**not verifiable**.
