---
title: "Cryptonator"
altTitle: 

appId: com.cryptonator.ios
idd: 1463793201
released: 2019-06-11
updated: 2019-09-21
version: "4.0"
score: 3.5
reviews: 6
size: 79710208
developerWebsite: https://www.cryptonator.com
repository: 
issue: 
icon: com.cryptonator.ios.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

