---
title: "Celsius - Crypto Wallet"
altTitle: 

appId: network.celsius.wallet
idd: 1387885523
released: 2018-06-20
updated: 2020-12-10
version: "4.6.1"
score: 4.32278
reviews: 663
size: 79927296
developerWebsite: https://celsius.network/app
repository: 
issue: 
icon: network.celsius.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

