---
title: "SafePal - Crypto Wallet BTC"
altTitle: 

appId: com.anbi.safepal
idd: 1449232593
released: 2019-03-01
updated: 2020-12-30
version: "2.5.1"
score: 3.78947
reviews: 19
size: 111534080
developerWebsite: https://www.safepal.io/
repository: 
issue: 
icon: com.anbi.safepal.jpg
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-15
reviewStale: false
signer: 
reviewArchive:


providerTwitter: iSafePal
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app appears to have disappeared from the App Store but
* we reviewed the [Android version](/android/io.safepal.wallet)
* their website links to
  [a different wallet on the App Store](/iphone/walletapp.safepal.io), so they
  probably had to switch for some technical reasons. If you know, let us know.