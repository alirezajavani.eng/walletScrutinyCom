---
title: "Multi Crypto Wallet－Freewallet"
altTitle: 

appId: mw.org.freewallet.app
idd: 1274003898
released: 2017-09-01
updated: 2020-12-09
version: "1.14.15"
score: 4.16055
reviews: 654
size: 45145088
developerWebsite: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

