---
title: "Exodus: Crypto Bitcoin Wallet"
altTitle: 

appId: exodus-movement.exodus
idd: 1414384820
released: 2019-03-23
updated: 2021-01-15
version: "21.1.15"
score: 4.74808
reviews: 5351
size: 27214848
developerWebsite: https://www.exodus.io/mobile
repository: 
issue: 
icon: exodus-movement.exodus.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

