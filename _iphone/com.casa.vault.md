---
title: "Casa App - Secure your Bitcoin"
altTitle: 

appId: com.casa.vault
idd: 1314586706
released: 2018-08-02
updated: 2021-01-16
version: "2.30.2"
score: 4.93939
reviews: 165
size: 42199040
developerWebsite: https://keys.casa
repository: 
issue: 
icon: com.casa.vault.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

