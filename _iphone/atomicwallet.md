---
title: "Atomic Wallet"
altTitle: 

appId: atomicwallet
idd: 1478257827
released: 2019-11-05
updated: 2021-01-13
version: "0.70.2"
score: 4.56013
reviews: 3351
size: 54727680
developerWebsite: https://atomicwallet.io/
repository: 
issue: 
icon: atomicwallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

