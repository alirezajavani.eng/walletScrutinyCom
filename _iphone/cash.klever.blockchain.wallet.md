---
title: "Klever Wallet"
altTitle: 

appId: cash.klever.blockchain.wallet
idd: 1525584688
released: 2020-08-26
updated: 2020-12-19
version: "4.1.1"
score: 4.55758
reviews: 165
size: 116711424
developerWebsite: https://klever.io
repository: 
issue: 
icon: cash.klever.blockchain.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

