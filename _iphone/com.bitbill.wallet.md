---
title: "Ownbit - Blockchain Wallet"
altTitle: 

appId: com.bitbill.wallet
idd: 1321798216
released: 2018-02-07
updated: 2021-01-13
version: "4.25.2"
score: 4.53191
reviews: 47
size: 106485760
developerWebsite: 
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

