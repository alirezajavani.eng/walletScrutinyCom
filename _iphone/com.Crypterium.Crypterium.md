---
title: "Crypterium | Bitcoin Wallet"
altTitle: 

appId: com.Crypterium.Crypterium
idd: 1360632912
released: 2018-03-26
updated: 2020-12-30
version: "1.14.4"
score: 4.53786
reviews: 898
size: 247984128
developerWebsite: https://cards.crypterium.com/visa
repository: 
issue: 
icon: com.Crypterium.Crypterium.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

