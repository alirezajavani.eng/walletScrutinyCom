---
title: "BitMart - Crypto Exchange"
altTitle: 

appId: com.bitmart.exchange
idd: 1396382871
released: 2018-08-02
updated: 2021-01-08
version: "2.4.3"
score: 3.68889
reviews: 45
size: 183453696
developerWebsite: https://www.bitmart.com/
repository: 
issue: 
icon: com.bitmart.exchange.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

