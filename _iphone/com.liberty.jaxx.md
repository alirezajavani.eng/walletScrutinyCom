---
title: "Jaxx Liberty Blockchain Wallet"
altTitle: 

appId: com.liberty.jaxx
idd: 1435383184
released: 2018-10-03
updated: 2020-08-27
version: "2.4.6"
score: 4.5311
reviews: 1045
size: 55321600
developerWebsite: https://jaxx.io
repository: 
issue: 
icon: com.liberty.jaxx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

