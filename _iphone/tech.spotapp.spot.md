---
title: "Buy Bitcoin - Spot Wallet app"
altTitle: 

appId: tech.spotapp.spot
idd: 1390560448
released: 2018-08-07
updated: 2021-01-08
version: "3.0.3"
score: 4.61564
reviews: 2123
size: 82849792
developerWebsite: https://spot-bitcoin.com
repository: 
issue: 
icon: tech.spotapp.spot.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

