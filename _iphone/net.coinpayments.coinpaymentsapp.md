---
title: "CoinPayments - Crypto Wallet"
altTitle: 

appId: net.coinpayments.coinpaymentsapp
idd: 1162855939
released: 2019-02-07
updated: 2020-07-27
version: "2.2.0"
score: 4.42856
reviews: 28
size: 134762496
developerWebsite: https://www.coinpayments.net/
repository: 
issue: 
icon: net.coinpayments.coinpaymentsapp.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

