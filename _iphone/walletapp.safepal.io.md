---
title: "SafePal Wallet"
altTitle: 

appId: walletapp.safepal.io
idd: 1548297139
released: 2021-01-11
updated: 2021-01-12
version: "2.5.3"
score: 5
reviews: 3
size: 111915008
developerWebsite: https://www.safepal.io/
repository: 
issue: 
icon: walletapp.safepal.io.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

