---
title: "XcelPay - Secure Crypto Wallet"
altTitle: 

appId: com.xcelpay.wallet
idd: 1461215417
released: 2019-05-26
updated: 2020-12-23
version: "2.13.6"
score: 4.88889
reviews: 9
size: 41398272
developerWebsite: http://xcelpay.io
repository: 
issue: 
icon: com.xcelpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

