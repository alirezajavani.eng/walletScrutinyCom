---
title: "ezDeFi-Crypto & Bitcoin Wallet"
altTitle: 

appId: com.ezdefi.nexty
idd: 1492046549
released: 2019-12-18
updated: 2020-12-30
version: "0.2.9"
score: 5
reviews: 3
size: 58009600
developerWebsite: https://ezdefi.com/
repository: 
issue: 
icon: com.ezdefi.nexty.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

