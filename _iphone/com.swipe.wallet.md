---
title: "Swipe Wallet"
altTitle: 

appId: com.swipe.wallet
idd: 1476726454
released: 2019-09-10
updated: 2021-01-15
version: "1.527"
score: 4.79645
reviews: 1184
size: 140695552
developerWebsite: https://swipe.io
repository: 
issue: 
icon: com.swipe.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

