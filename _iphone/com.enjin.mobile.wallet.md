---
title: "Enjin Crypto Blockchain Wallet"
altTitle: 

appId: com.enjin.mobile.wallet
idd: 1349078375
released: 2018-03-12
updated: 2020-12-15
version: "1.11.0"
score: 4.63679
reviews: 424
size: 42930176
developerWebsite: https://enjin.io/products/wallet
repository: 
issue: 
icon: com.enjin.mobile.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

